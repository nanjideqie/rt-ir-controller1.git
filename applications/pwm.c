#include <rtthread.h>
#include "board.h"

/**********************************************************************************/
/********************************  PWM_DEV_NAME ***********************************/
/*** timer3 pwm  *** timer4 pwm *** timer5 pwm ***  lpwm0 *** lpwm1 *** lpwm2 ***/
/*** t3pwm       *** t4pwm      *** t5pwm      ***  lpwm0 *** lpwm1 *** lpwm2 ***/
/**********************************************************************************/
#define PWM_DEV_NAME        "t5pwm"  /* PWM设备名称 */
#define PWM_DEV_CHANNEL     1       /* PWM通道 */
struct rt_device_pwm *pwm_dev;      /* PWM设备句柄 */
ALIGN(RT_ALIGN_SIZE)
static uint8_t PWM_Thread_Stack[1024];
static void PWM_Thread_Entry(void *para);
static struct rt_thread pwm_thread;
rt_uint32_t period, pulse;
void Pwm_Init(void){
    period = 26316;    /* 周期 = 1M/period kHz */
    pulse = 13158;          /* PWM脉冲宽度值(0 - period) */
    pwm_dev = (struct rt_device_pwm *)rt_device_find(PWM_DEV_NAME);
    RT_ASSERT(pwm_dev != RT_NULL);
    /* 设置PWM周期和脉冲宽度 */
    rt_pwm_set(pwm_dev, PWM_DEV_CHANNEL, period, pulse);
    /* 不使能设备 */
    //rt_pwm_disable(pwm_dev, PWM_DEV_CHANNEL);
}

static int pwm_sample(void){
    Pwm_Init();
    //rt_pwm_set(pwm_dev, PWM_DEV_CHANNEL, period, 13158);

}
//INIT_APP_EXPORT(pwm_sample);
//MSH_CMD_EXPORT(pwm_sample, pwm sample);
