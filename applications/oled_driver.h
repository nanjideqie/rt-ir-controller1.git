/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-12-06     hcszh       the first version
 */
#ifndef APPLICATIONS_OLED_DRIVER_H_
#define APPLICATIONS_OLED_DRIVER_H_

#include "rtthread.h"

#define OLED_SCL_PIN "PA.0"
#define OLED_SDA_PIN "PA.1"
#define OLED_RST_PIN "PE.4"
#define OLED_DC_PIN "PA.6"

extern int oled_setup();
extern void OLED_ShowString(rt_uint8_t x,rt_uint8_t y,const char *chr,rt_uint8_t sizey);
extern void OLED_Clear(void);

#endif /* APPLICATIONS_OLED_DRIVER_H_ */
