/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-12-07     hcszh       the first version
 */
#ifndef APPLICATIONS_REALTIME_H_
#define APPLICATIONS_REALTIME_H_
#include <rtthread.h>
extern struct rt_event rtc_1s_event;
extern int rtc_setup(rt_uint32_t hour, rt_uint32_t minute, rt_uint32_t second);

#endif /* APPLICATIONS_REALTIME_H_ */
