/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-12-12     hcszh       the first version
 */
#ifndef APPLICATIONS_IRRX_APP_H_
#define APPLICATIONS_IRRX_APP_H_

extern int tim1_sample(void);


#endif /* APPLICATIONS_IRRX_APP_H_ */
